

/*==============================================================*/
/* Table: CHAIRS                                                */
/*==============================================================*/
create table CHAIRS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   MODEL_ID             INT4                 not null,
   NOTE                 TEXT                 null,
   constraint PK_CHAIRS primary key (ID)
);

/*==============================================================*/
/* Index: CHAIRS_INDEX                                          */
/*==============================================================*/
create unique index CHAIRS_INDEX on CHAIRS (
ID
);

/*==============================================================*/
/* Table: COLLEGES                                              */
/*==============================================================*/
create table COLLEGES (
   ID                   SERIAL not null,
   NAME                 CHAR(100)            not null,
   CITY                 CHAR(20)             not null,
   COUNTRY              CHAR(30)             not null,
   DESCRIPTION          TEXT                 null,
   constraint PK_COLLEGES primary key (ID)
);

/*==============================================================*/
/* Index: COLLEGES_INDEX                                        */
/*==============================================================*/
create unique index COLLEGES_INDEX on COLLEGES (
ID
);

/*==============================================================*/
/* Table: COMPANIES                                             */
/*==============================================================*/
create table COMPANIES (
   ID                   SERIAL not null,
   NAME                 CHAR(50)             not null,
   INDUSTRY             CHAR(50)             null,
   LOCATION             CHAR(50)             not null,
   NOTE                 TEXT                 null,
   constraint PK_COMPANIES primary key (ID)
);

/*==============================================================*/
/* Index: COMPANIES_INDEX                                       */
/*==============================================================*/
create unique index COMPANIES_INDEX on COMPANIES (
ID
);

/*==============================================================*/
/* Table: COMPUTERS                                             */
/*==============================================================*/
create table COMPUTERS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   MOTHERBOARD          CHAR(30)             not null,
   RAM                  CHAR(20)             not null,
   PROCESSOR            CHAR(30)             not null,
   HARD_DRIVE           CHAR(30)             not null,
   AUDIO                CHAR(20)             null,
   VIDEO                CHAR(20)             null,
   KEYBOARD             CHAR(20)             null,
   MICE                 CHAR(20)             null,
   NOTE                 TEXT                 null,
   constraint PK_COMPUTERS primary key (ID)
);

/*==============================================================*/
/* Index: COMPUTERS_INDEX                                       */
/*==============================================================*/
create unique index COMPUTERS_INDEX on COMPUTERS (
ID
);

/*==============================================================*/
/* Table: CONTACTS                                              */
/*==============================================================*/
create table CONTACTS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   MOBILE_PHONE         CHAR(10)             not null,
   HOME_PHONE           CHAR(10)             null,
   OTHER_PHONE          CHAR(10)             null,
   EMAIL                CHAR(40)             not null,
   SKYPE                CHAR(40)             null,
   IM                   CHAR(30)             null,
   NOTE                 TEXT                 null,
   constraint PK_CONTACTS primary key (ID)
);

/*==============================================================*/
/* Index: CONTACTS_INDEX                                        */
/*==============================================================*/
create unique index CONTACTS_INDEX on CONTACTS (
ID
);

/*==============================================================*/
/* Table: EDUCATIONS                                            */
/*==============================================================*/
create table EDUCATIONS (
   ID                   SERIAL not null,
   COLLEGE_ID           INT4                 not null,
   EMPLOYEE_ID          INT4                 not null,
   SPECIALITY           CHAR(100)            not null,
   DEGREE               CHAR(10)             not null,
   START_DATE           DATE                 not null,
   END_DATE             DATE                 null,
   constraint PK_EDUCATIONS primary key (ID)
);

/*==============================================================*/
/* Index: EDUCATIONS_INDEX                                      */
/*==============================================================*/
create unique index EDUCATIONS_INDEX on EDUCATIONS (
ID
);

/*==============================================================*/
/* Table: EMPLOYEES                                             */
/*==============================================================*/
create table EMPLOYEES (
   ID                   SERIAL not null,
   CURRENT_POSSITION    INT2                 not null,
   STATUS               BOOL                 not null,
   FIRST_NAME           VARCHAR(20)          not null,
   MIDDLE_NAME          VARCHAR(30)          not null,
   SURNAME              VARCHAR(40)          not null,
   NOTE                 TEXT                 null,
   constraint PK_EMPLOYEES primary key (ID)
);

/*==============================================================*/
/* Index: EMPLOYEES_INDEX                                       */
/*==============================================================*/
create unique index EMPLOYEES_INDEX on EMPLOYEES (
ID
);

/*==============================================================*/
/* Table: EMPLOYEES_INFO                                        */
/*==============================================================*/
create table EMPLOYEES_INFO (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 null,
   BIRTHDAY             DATE                 null,
   CHILDS               INT4                 null,
   constraint PK_EMPLOYEES_INFO primary key (ID)
);

/*==============================================================*/
/* Index: EMPLOYEES_INFO_INDEXES                                */
/*==============================================================*/
create unique index EMPLOYEES_INFO_INDEXES on EMPLOYEES_INFO (
ID
);

/*==============================================================*/
/* Table: EXPERIENCE                                            */
/*==============================================================*/
create table EXPERIENCE (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   PROJECT_ID           INT4                 not null,
   START_DATE           DATE                 not null,
   END_DATE             DATE                 null,
   DUTIES               TEXT                 not null,
   constraint PK_EXPERIENCE primary key (ID)
);

/*==============================================================*/
/* Index: EXPERIENCE_INDEX                                      */
/*==============================================================*/
create unique index EXPERIENCE_INDEX on EXPERIENCE (
ID
);

/*==============================================================*/
/* Table: EXPERIENCE_SKILLS                                     */
/*==============================================================*/
create table EXPERIENCE_SKILLS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 null,
   EXPERIENCE_ID        INT4                 null,
   SKILL_DESCRIPTION_ID INT4                 null,
   constraint PK_EXPERIENCE_SKILLS primary key (ID)
);

/*==============================================================*/
/* Index: EXPERIENCE_SKILLS_INDEX                               */
/*==============================================================*/
create unique index EXPERIENCE_SKILLS_INDEX on EXPERIENCE_SKILLS (
ID
);

/*==============================================================*/
/* Table: JOBS                                                  */
/*==============================================================*/
create table JOBS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   TEAM_ID              INT4                 null,
   STATUS               BOOL                 not null,
   NOTE                 TEXT                 null,
   constraint PK_JOBS primary key (ID)
);

/*==============================================================*/
/* Index: JOBS_INDEX                                            */
/*==============================================================*/
create unique index JOBS_INDEX on JOBS (
ID
);

/*==============================================================*/
/* Table: LAPTOPS                                               */
/*==============================================================*/
create table LAPTOPS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   NAME                 CHAR(30)             not null,
   MODEL                CHAR(30)             not null,
   NOTE                 TEXT                 null,
   constraint PK_LAPTOPS primary key (ID)
);

/*==============================================================*/
/* Index: LAPTOPS_INDEX                                         */
/*==============================================================*/
create unique index LAPTOPS_INDEX on LAPTOPS (
ID
);

/*==============================================================*/
/* Table: LOCATIONS                                              */
/*==============================================================*/
create table LOCATIONS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   OFFICE_ID            INT4                 not null,
   ROOM                 VARCHAR(20)          not null,
   START_DATE           DATE                 not null,
   NOTE                 TEXT                 null,
   constraint PK_LOCATION primary key (ID)
);

/*==============================================================*/
/* Index: LOCATION_INDEX                                        */
/*==============================================================*/
create unique index LOCATION_INDEX on LOCATIONS (
ID
);

/*==============================================================*/
/* Table: MODELS_OF_CHAIRS                                      */
/*==============================================================*/
create table MODELS_OF_CHAIRS (
   ID                   SERIAL not null,
   MODEL                CHAR(20)             not null,
   PICTURE_PATH         CHAR(30)             not null,
   NOTE                 TEXT                 null,
   constraint PK_MODELS_OF_CHAIRS primary key (ID)
);

/*==============================================================*/
/* Index: MODEL_OF_CHAIRS_INDEX                                 */
/*==============================================================*/
create unique index MODEL_OF_CHAIRS_INDEX on MODELS_OF_CHAIRS (
ID
);

/*==============================================================*/
/* Table: MODELS_OF_TABLES                                      */
/*==============================================================*/
create table MODELS_OF_TABLES (
   ID                   SERIAL not null,
   MODEL                CHAR(20)             not null,
   PICTURE_PATH         CHAR(30)             not null,
   NOTE                 TEXT                 null,
   constraint PK_MODELS_OF_TABLES primary key (ID)
);

/*==============================================================*/
/* Index: MODELS_OF_TABLES_INDEX                                */
/*==============================================================*/
create unique index MODELS_OF_TABLES_INDEX on MODELS_OF_TABLES (
ID
);

/*==============================================================*/
/* Table: MODELS_OF_UPSES                                       */
/*==============================================================*/
create table MODELS_OF_UPSES (
   ID                   SERIAL not null,
   MODEL                CHAR(20)             not null,
   PICTURE_PATH         CHAR(30)             not null,
   NOTE                 TEXT                 null,
   constraint PK_MODELS_OF_UPSES primary key (ID)
);

/*==============================================================*/
/* Index: MODELS_OF_UPSES_INDEX                                 */
/*==============================================================*/
create unique index MODELS_OF_UPSES_INDEX on MODELS_OF_UPSES (
ID
);

/*==============================================================*/
/* Table: MONITORS                                              */
/*==============================================================*/
create table MONITORS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   NAME                 CHAR(30)             not null,
   MODEL                CHAR(30)             not null,
   NOTE                 TEXT                 null,
   constraint PK_MONITORS primary key (ID)
);

/*==============================================================*/
/* Index: MONITORS_INDEX                                        */
/*==============================================================*/
create unique index MONITORS_INDEX on MONITORS (
ID
);

/*==============================================================*/
/* Table: OFFICES                                               */
/*==============================================================*/
create table OFFICES (
   ID                   SERIAL not null,
   NAME                 CHAR(30)             not null,
   ADDRESS              VARCHAR(30)          not null,
   EMAIL                CHAR(40)             not null,
   NOTES                TEXT                 null,
   constraint PK_OFFICES primary key (ID)
);

/*==============================================================*/
/* Index: OFFICCES_INDEX                                        */
/*==============================================================*/
create unique index OFFICCES_INDEX on OFFICES (
ID
);

/*==============================================================*/
/* Table: PERMISSIONS                                           */
/*==============================================================*/
create table PERMISSIONS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 null,
   ROLE_ID              INT4                 null,
   constraint PK_PERMISSIONS primary key (ID)
);

/*==============================================================*/
/* Index: PERMISSIONS_INDEXEX                                   */
/*==============================================================*/
create unique index PERMISSIONS_INDEXEX on PERMISSIONS (
ID
);

/*==============================================================*/
/* Table: POSITIONS                                             */
/*==============================================================*/
create table POSITIONS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   DESCRIPTION_ID       INT4                 not null,
   DATE                 DATE                 not null,
   constraint PK_POSITIONS primary key (ID)
);

/*==============================================================*/
/* Index: POSITIONS_INDEX                                       */
/*==============================================================*/
create unique index POSITIONS_INDEX on POSITIONS (
ID
);

/*==============================================================*/
/* Table: POSITIONS_DESCRIPTION                                 */
/*==============================================================*/
create table POSITIONS_DESCRIPTION (
   ID                   SERIAL not null,
   NAME                 CHAR(30)             not null,
   DESCRIPTION          TEXT                 null,
   constraint PK_POSITIONS_DESCRIPTION primary key (ID)
);

/*==============================================================*/
/* Index: POSITIONS_DESCRIPTION_INDEX                           */
/*==============================================================*/
create unique index POSITIONS_DESCRIPTION_INDEX on POSITIONS_DESCRIPTION (
ID
);

/*==============================================================*/
/* Table: PROJECTS                                              */
/*==============================================================*/
create table PROJECTS (
   ID                   SERIAL not null,
   COMPANY_ID           INT4                 not null,
   TEAM_ID              INT4                 null,
   NAME                 CHAR(50)             not null,
   NOTE                 TEXT                 null,
   constraint PK_PROJECTS primary key (ID)
);

/*==============================================================*/
/* Index: PROJECT_INDEX                                         */
/*==============================================================*/
create unique index PROJECT_INDEX on PROJECTS (
ID
);

/*==============================================================*/
/* Table: ROLES                                                 */
/*==============================================================*/
create table ROLES (
   ID                   SERIAL not null,
   NAME                 VARCHAR(30)          not null,
   NOTE                 TEXT                 null,
   constraint PK_ROLES primary key (ID)
);

/*==============================================================*/
/* Index: ROLES_INDEXES                                         */
/*==============================================================*/
create unique index ROLES_INDEXES on ROLES (
ID
);

/*==============================================================*/
/* Table: SKILLS                                                */
/*==============================================================*/
create table SKILLS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   SKILL_DESCRIPTION_ID INT4                 not null,
   SKILL_LEVEL_ID       INT4                 not null,
   LAST_USED            DATE                 not null,
   EXPERIENCE           INT2                 not null,
   constraint PK_SKILLS primary key (ID)
);

/*==============================================================*/
/* Index: SKILLS_INDEXES                                        */
/*==============================================================*/
create unique index SKILLS_INDEXES on SKILLS (
ID
);

/*==============================================================*/
/* Table: SKILLS_DESCRIPTION                                    */
/*==============================================================*/
create table SKILLS_DESCRIPTION (
   ID                   SERIAL not null,
   GROUP_ID             INT4                 not null,
   NAME                 CHAR(30)             not null,
   DESCRIPTION          TEXT                 null,
   constraint PK_SKILLS_DESCRIPTION primary key (ID)
);

/*==============================================================*/
/* Index: SKILLS_DESCRIPTION_INDEX                              */
/*==============================================================*/
create unique index SKILLS_DESCRIPTION_INDEX on SKILLS_DESCRIPTION (
ID
);

/*==============================================================*/
/* Table: SKILLS_GROUP                                          */
/*==============================================================*/
create table SKILLS_GROUP (
   ID                   SERIAL not null,
   NAME                 CHAR(30)             not null,
   DESCRIPTION          TEXT                 null,
   constraint PK_SKILLS_GROUP primary key (ID)
);

/*==============================================================*/
/* Index: SKILLS_GROUP_INDEX                                    */
/*==============================================================*/
create unique index SKILLS_GROUP_INDEX on SKILLS_GROUP (
ID
);

/*==============================================================*/
/* Table: SKILLS_LEVEL                                          */
/*==============================================================*/
create table SKILLS_LEVEL (
   ID                   SERIAL not null,
   NAME                 CHAR(30)             not null,
   DESCRIPTION          TEXT                 null,
   constraint PK_SKILLS_LEVEL primary key (ID)
);

/*==============================================================*/
/* Index: SKILLS_LEVEL_INDEX                                    */
/*==============================================================*/
create unique index SKILLS_LEVEL_INDEX on SKILLS_LEVEL (
ID
);

/*==============================================================*/
/* Table: TABLES                                                */
/*==============================================================*/
create table TABLES (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   MODEL_ID             INT4                 not null,
   NOTE                 TEXT                 null,
   constraint PK_TABLES primary key (ID)
);

/*==============================================================*/
/* Index: TABLES_INDEX                                          */
/*==============================================================*/
create unique index TABLES_INDEX on TABLES (
ID
);

/*==============================================================*/
/* Table: TEAMS                                                 */
/*==============================================================*/
create table TEAMS (
   ID                   SERIAL not null,
   PROJECT_MANAGER_ID   INT4                 null,
   TEAM_LEADER_ID       INT4                 null,
   NAME                 CHAR(30)             not null,
   NOTE                 TEXT                 null,
   constraint PK_TEAMS primary key (ID)
);

/*==============================================================*/
/* Index: TEAMS_INDEX                                           */
/*==============================================================*/
create unique index TEAMS_INDEX on TEAMS (
ID
);

/*==============================================================*/
/* Table: UPSES                                                 */
/*==============================================================*/
create table UPSES (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 not null,
   MODEL_ID             INT4                 not null,
   NOTE                 TEXT                 null,
   constraint PK_UPSES primary key (ID)
);

/*==============================================================*/
/* Index: UPSES_INDEX                                           */
/*==============================================================*/
create unique index UPSES_INDEX on UPSES (
ID
);

/*==============================================================*/
/* Table: VACATIONS                                             */
/*==============================================================*/
create table VACATIONS (
   ID                   SERIAL not null,
   EMPLOYEE_ID          INT4                 null,
   START_DATE           DATE                 not null,
   END_DATE             DATE                 not null,
   NOTE                 TEXT                 null,
   constraint PK_VACATIONS primary key (ID)
);

/*==============================================================*/
/* Index: VACATION_INDEX                                        */
/*==============================================================*/
create unique index VACATION_INDEX on VACATIONS (
ID
);

alter table CHAIRS
   add constraint FK_CHAIRS_CHAIRS_TO_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table CHAIRS
   add constraint FK_CHAIRS_CHAIRS_TO_MODELS_O foreign key (MODEL_ID)
      references MODELS_OF_CHAIRS (ID)
      on delete restrict on update restrict;

alter table COMPUTERS
   add constraint FK_COMPUTER_COMPUTERS_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table CONTACTS
   add constraint FK_CONTACTS_CONTACTS__EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table EDUCATIONS
   add constraint FK_EDUCATIO_EDUCATION_COLLEGES foreign key (COLLEGE_ID)
      references COLLEGES (ID)
      on delete restrict on update restrict;

alter table EDUCATIONS
   add constraint FK_EDUCATIO_EDUCATION_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table EMPLOYEES_INFO
   add constraint FK_EMPLOYEE_EMPLOYEES_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table EXPERIENCE
   add constraint FK_EXPERIEN_EXPERIENC_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table EXPERIENCE
   add constraint FK_EXPERIEN_EXPERIENC_PROJECTS foreign key (PROJECT_ID)
      references PROJECTS (ID)
      on delete restrict on update restrict;

alter table EXPERIENCE_SKILLS
   add constraint FK_EXPERIEN_EXPERIENC_EXPERIEN foreign key (EXPERIENCE_ID)
      references EXPERIENCE (ID)
      on delete restrict on update restrict;

alter table EXPERIENCE_SKILLS
   add constraint FK_EXPERIEN_EXPERIENC_SKILLS_D foreign key (SKILL_DESCRIPTION_ID)
      references SKILLS_DESCRIPTION (ID)
      on delete restrict on update restrict;

alter table EXPERIENCE_SKILLS
   add constraint FK_EXPERIEN_REFERENCE_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table JOBS
   add constraint FK_JOBS_JOBS_TO_E_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table JOBS
   add constraint FK_JOBS_JOBS_TO_T_TEAMS foreign key (TEAM_ID)
      references TEAMS (ID)
      on delete restrict on update restrict;

alter table LAPTOPS
   add constraint FK_LAPTOPS_LAPTOPS_T_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table LOCATIONS
   add constraint FK_LOCATION_LOCATION__EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table LOCATIONS
   add constraint FK_LOCATION_LOCATION__OFFICES foreign key (OFFICE_ID)
      references OFFICES (ID)
      on delete restrict on update restrict;

alter table MONITORS
   add constraint FK_MONITORS_MONITORS__EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table PERMISSIONS
   add constraint FK_PERMISSI_PERMISSIO_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table PERMISSIONS
   add constraint FK_PERMISSI_PERMISSIO_ROLES foreign key (ROLE_ID)
      references ROLES (ID)
      on delete restrict on update restrict;

alter table POSITIONS
   add constraint FK_POSITION_POSITIONS_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table POSITIONS
   add constraint FK_POSITION_POSITION__POSITION foreign key (DESCRIPTION_ID)
      references POSITIONS_DESCRIPTION (ID)
      on delete restrict on update restrict;

alter table PROJECTS
   add constraint FK_PROJECTS_PROJECTS__COMPANIE foreign key (COMPANY_ID)
      references COMPANIES (ID)
      on delete restrict on update restrict;

alter table PROJECTS
   add constraint FK_PROJECTS_PROJECTS__TEAMS foreign key (TEAM_ID)
      references TEAMS (ID)
      on delete restrict on update restrict;

alter table SKILLS
   add constraint FK_SKILLS_SKILLS_TO_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table SKILLS
   add constraint FK_SKILLS_SKILLS_TO_SKILLS_D foreign key (SKILL_DESCRIPTION_ID)
      references SKILLS_DESCRIPTION (ID)
      on delete restrict on update restrict;

alter table SKILLS
   add constraint FK_SKILLS_SKILLS_TO_SKILLS_L foreign key (SKILL_LEVEL_ID)
      references SKILLS_LEVEL (ID)
      on delete restrict on update restrict;

alter table SKILLS_DESCRIPTION
   add constraint FK_SKILLS_D_SKILL_GRO_SKILLS_G foreign key (GROUP_ID)
      references SKILLS_GROUP (ID)
      on delete restrict on update restrict;

alter table TABLES
   add constraint FK_TABLES_TABLES_TO_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table TABLES
   add constraint FK_TABLES_TABLES_TO_MODELS_O foreign key (MODEL_ID)
      references MODELS_OF_TABLES (ID)
      on delete restrict on update restrict;

alter table TEAMS
   add constraint FK_TEAMS_TEAM_LEAD_TO_EMPLOYEE foreign key (TEAM_LEADER_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table TEAMS
   add constraint FK_TEAMS_PM_TO_EMPLOYEE foreign key (PROJECT_MANAGER_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table UPSES
   add constraint FK_UPSES_UPSES_TO__EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;

alter table UPSES
   add constraint FK_UPSES_UPSES_TO__MODELS_O foreign key (MODEL_ID)
      references MODELS_OF_UPSES (ID)
      on delete restrict on update restrict;

alter table VACATIONS
   add constraint FK_VACATION_VACATIONS_EMPLOYEE foreign key (EMPLOYEE_ID)
      references EMPLOYEES (ID)
      on delete restrict on update restrict;


/*==============================================================*/
/* SET EMPLOYEE DATA						*/
/*==============================================================*/

INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (2, '1', 'Evan', '', 'Petrovich', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (2, '1', 'Val', 'Alibabaevich', 'Kudryashkin', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (3, '1', 'Ann', '', 'McKeyn', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (5, '1', 'Barbara', '', 'Streisand', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (5, '1', 'Andrew', 'Alexandrovich', 'Pupkin', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (5, '0', 'Nick', 'Sergeevich', 'Byakin', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (2, '1', 'Marry', 'Vasilivna', 'Cukerberg', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (1, '1', 'Allan', 'Juniour', 'Smith', NULL);

INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (1, '1', 'Andrey', 'Nikolaevich', 'Kuznecov', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (1, '1', 'Valentina', 'Fedorovna', 'Balalaika', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (1, '1', 'Marry', 'Alekseevna', 'Murzik', NULL);
INSERT INTO employees (current_possition, status, first_name, middle_name, surname, note) VALUES (1, '1', 'Fedor', 'Vasilievich', 'Golohvostov', NULL);


/*==============================================================*/
/* SET CONTACTS DATA						*/
/*==============================================================*/


INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (2, '43213442  ', '524221    ', '54552111  ', 'petrovich@thecompany.com                ', 'evan_petr                               ', '332335                        ', NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (4, '1125675   ', '44574253  ', NULL, 'kudryashkin@thecompany.com              ', 'saint_val                               ', '152754                        ', NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (3, '76467567  ', '4556799   ', NULL, 'mcukerberg@thecompany.com               ', 'just_marry                              ', '1234567                       ', NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (1, '343455324 ', '34525675  ', NULL, 'smith@thecompany.com                    ', 'smiththegreate                          ', '33221177                      ', NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (5, '6342653   ', '64443234  ', NULL, 'ann@thecompany.com                      ', 'mcKeyn                                  ', NULL, NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (6, '76245648  ', '577535345 ', '25524677  ', 'uuuu@thecompany.com                     ', 'bstr                                    ', NULL, NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (8, '098-443-22', '3809455352', NULL, 'byaka@thecompany.com                    ', '                                        ', '553133                        ', NULL);
INSERT INTO contacts (employee_id, mobile_phone, home_phone, other_phone, email, skype, im, note) VALUES (7, '048-434-24', '3809776765', NULL, 'andryusha@thecompany.com                ', '                                        ', NULL, NULL);


/*==============================================================*/
/* SET POSITIONS_DESCRIPTION DATA				*/
/*==============================================================*/

INSERT INTO positions_description (name, description) VALUES ('Software Engeneer             ', NULL);
INSERT INTO positions_description (name, description) VALUES ('Principal Engeneer            ', NULL);
INSERT INTO positions_description (name, description) VALUES ('Senior Software Engeneer      ', NULL);
INSERT INTO positions_description (name, description) VALUES ('Database Administrator        ', NULL);
INSERT INTO positions_description (name, description) VALUES ('System Administrator          ', NULL);
INSERT INTO positions_description (name, description) VALUES ('Quality Asistant              ', NULL);


/*==============================================================*/
/* SET POSITIONS DATA						*/
/*==============================================================*/

INSERT INTO positions (employee_id, description_id, date) VALUES (1, 1, '2007-05-16');
INSERT INTO positions (employee_id, description_id, date) VALUES (1, 2, '2012-01-11');
INSERT INTO positions (employee_id, description_id, date) VALUES (2, 3, '2011-01-25');
INSERT INTO positions (employee_id, description_id, date) VALUES (3, 4, '2007-07-01');
INSERT INTO positions (employee_id, description_id, date) VALUES (4, 4, '2007-07-05');
INSERT INTO positions (employee_id, description_id, date) VALUES (5, 4, '2007-07-19');
INSERT INTO positions (employee_id, description_id, date) VALUES (5, 1, '2009-11-09');
INSERT INTO positions (employee_id, description_id, date) VALUES (6, 3, '2010-01-21');
INSERT INTO positions (employee_id, description_id, date) VALUES (7, 4, '2011-09-11');
INSERT INTO positions (employee_id, description_id, date) VALUES (8, 1, '2005-11-25');


/*==============================================================*/
/* SET SKILLS_GROUP DATA					*/
/*==============================================================*/

INSERT INTO skills_group (name, description) VALUES ('Operating Systems             ', NULL);
INSERT INTO skills_group (name, description) VALUES ('RDBMS                         ', NULL);
INSERT INTO skills_group (name, description) VALUES ('Programming languages         ', NULL);
INSERT INTO skills_group (name, description) VALUES ('Technologies                  ', NULL);


/*==============================================================*/
/* SET SKILLS_LEVEL DATA					*/
/*==============================================================*/

INSERT INTO skills_level (name, description) VALUES ('Basic                         ', 'Less then 1000 hours');
INSERT INTO skills_level (name, description) VALUES ('Intermediate                  ', 'More then 1000 hours but less than 3000 hours');
INSERT INTO skills_level (name, description) VALUES ('Advanced                      ', 'More then 3000 hours but less than 5000 hours');
INSERT INTO skills_level (name, description) VALUES ('Expert                        ', 'More then 5000 hours');


/*==============================================================*/
/* SET SKILLS DESCRIPTION                                       */
/*==============================================================*/

INSERT INTO skills_description (group_id, name) VALUES (3, 'C++                           ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'C                             ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Java                          ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Php                           ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'JavaScript                    ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Delphi                        ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Python                        ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'C#                            ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Shell                         ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Ruby                          ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Assembly                      ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'SQL                           ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Perl                          ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'ASP                           ');
INSERT INTO skills_description (group_id, name) VALUES (3, 'Lua                           ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'MySQL                         ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'PostgreSQL                    ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'SQLite                        ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'MSql                          ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'Microsoft Access              ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'Oracle                        ');
INSERT INTO skills_description (group_id, name) VALUES (2, 'MariaDB                       ');
INSERT INTO skills_description (group_id, name) VALUES (1, 'Linux                         ');
INSERT INTO skills_description (group_id, name) VALUES (1, 'Windows                       ');
INSERT INTO skills_description (group_id, name) VALUES (4, 'XML                           ');
INSERT INTO skills_description (group_id, name) VALUES (4, 'JSON                          ');
INSERT INTO skills_description (group_id, name) VALUES (4, '.NET                          ');
INSERT INTO skills_description (group_id, name) VALUES (4, 'COM                           ');
INSERT INTO skills_description (group_id, name) VALUES (4, 'OLE                           ');
INSERT INTO skills_description (group_id, name) VALUES (4, 'Velocity                      ');
INSERT INTO skills_description (group_id, name) VALUES (4, 'XSLT                          ');


/*==============================================================*/
/* SET SKILLS DATA						*/
/*==============================================================*/
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 2, 2, '2011-04-20', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 1, 1, '2010-03-12', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 3, 1, '2012-05-04', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 11, 3, '2013-04-27', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 12, 4, '2005-01-22', 4);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 23, 2, '2010-11-14', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 24, 2, '2013-10-07', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 31, 3, '2012-08-22', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (1, 26, 2, '2011-09-30', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 1, 1, '2011-06-23', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 2, 1, '2011-07-12', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 3, 1, '2010-02-24', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 5, 3, '2011-01-07', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 12, 2, '2008-06-02', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 14, 3, '2008-10-15', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 17, 3, '2011-11-02', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 21, 3, '2010-02-12', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 23, 1, '2009-07-01', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 24, 1, '2010-11-17', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (2, 31, 3, '2011-03-12', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 7, 1, '2011-06-23', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 8, 1, '2011-07-12', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 3, 1, '2010-02-24', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 9, 2, '2011-01-07', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 10, 2, '2008-06-02', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 11, 3, '2008-11-15', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 17, 2, '2011-11-02', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 20, 2, '2010-02-12', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 21, 2, '2009-07-01', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 29, 1, '2010-11-17', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 31, 3, '2011-03-12', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 15, 2, '2013-02-23', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 1, 4, '2013-11-25', 4);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (3, 6, 1, '2012-07-31', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 22, 2, '2010-01-12', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 25, 2, '2007-07-08', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 21, 1, '2013-10-12', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 30, 1, '2011-02-11', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 12, 1, '2013-08-20', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 4, 3, '2013-10-29', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (4, 9, 1, '2012-09-30', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 20, 1, '2011-05-12', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 23, 1, '2011-06-18', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 21, 2, '2010-11-02', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 8, 2, '2013-01-10', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 18, 2, '2012-09-24', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 4, 2, '2012-10-22', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 9, 2, '2010-11-15', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 14, 1, '2009-12-19', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (5, 19, 4, '2013-12-11', 4);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 1, 3, '2010-02-15', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 2, 3, '2011-05-10', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 3, 3, '2012-01-16', 4);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 4, 4, '2013-07-17', 5);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 10, 2, '2009-03-22', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 13, 1, '2009-10-25', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 14, 1, '2010-11-06', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 18, 2, '2011-10-02', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 16, 2, '2011-09-03', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 19, 3, '2008-06-07', 5);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 11, 4, '2012-07-15', 7);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 25, 1, '2012-03-14', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 21, 2, '2011-09-17', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 26, 2, '2013-02-01', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 27, 2, '2012-06-21', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 30, 3, '2013-08-23', 4);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (6, 31, 1, '2013-07-11', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 1, 2, '2013-08-11', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 6, 1, '2013-04-14', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 7, 1, '2011-03-16', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 14, 3, '2012-02-17', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 13, 4, '2011-07-24', 4);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 26, 2, '2011-01-16', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 24, 2, '2010-11-02', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 30, 3, '2009-10-08', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (7, 27, 2, '2010-09-30', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 4, 1, '2013-02-13', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 7, 1, '2013-05-16', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 8, 1, '2012-03-23', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 9, 1, '2012-07-02', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 10, 2, '2009-08-01', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 11, 3, '2008-09-16', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 12, 2, '2010-11-11', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 25, 1, '2013-06-17', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 23, 2, '2010-03-21', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 29, 1, '2011-10-16', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 31, 3, '2012-11-10', 3);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 15, 2, '2012-08-27', 2);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 16, 1, '2013-05-22', 1);
INSERT INTO skills (employee_id, skill_description_id, skill_level_id, last_used, experience) VALUES (8, 17, 1, '2012-07-21', 1);

/*==============================================================*/
/* SET COLLEGES DATA                                            */
/*==============================================================*/
INSERT INTO colleges (name, city, country) VALUES ('Odessa National Polytechnic University', 'Odessa', 'Ukraine');
INSERT INTO colleges (name, city, country) VALUES ('Odessa National University', 'Odessa', 'Ukraine');
INSERT INTO colleges (name, city, country) VALUES ('Odessa National Academy of Telecommunications', 'Odessa', 'Ukraine');
INSERT INTO colleges (name, city, country) VALUES ('Kyiv Polytechnic Institute', 'Kyiv', 'Ukraine');

/*==============================================================*/
/* SET EDUCATIONS DATA                                            */
/*==============================================================*/
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (1, 1, 'Data evaluation instruments; measuring units and circuits', 'Master', '2002-08-01', '2008-05-01');
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (2, 2, 'Do nothing', 'Master', '2006-08-01', null);
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (3, 3, 'Units and circuits', 'Bachelor', '2009-08-01', '2013-05-01');
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (4, 4, 'Data', 'Specialist', '2006-08-01', '2011-05-01');
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (1, 5, 'Computer systems and networks', 'Master', '2004-08-01', '2010-05-01');
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (1, 6, 'SPO', 'Specialist', '2012-08-01', null);
INSERT INTO educations(college_id, employee_id, speciality, degree, start_date, end_date) VALUES (3, 7, 'Data evaluation instruments; measuring units and circuits', 'Master', '2002-08-01', '2008-05-01');

/*==============================================================*/
/* SET COMPANIES DATA                                           */
/*==============================================================*/
INSERT INTO companies (name, industry, location) VALUES ('The Product Engine', 'IT � Software Systems Design', 'Saratoga, CA');
INSERT INTO companies (name, industry, location) VALUES ('Luxoft', 'IT Service Provider', 'Odessa, Ukraine');
INSERT INTO companies (name, industry, location) VALUES ('Baymark Ukraine', 'Development of software and hardware systems', 'Odessa, Ukraine');
INSERT INTO companies (name, industry, location) VALUES ('Lohika', 'Accelerate innovation', 'Odessa, Ukraine');
INSERT INTO companies (name, industry, location) VALUES ('Provectus IT', 'Advanced solutions', 'Odessa, Ukraine');
INSERT INTO companies (name, industry, location) VALUES ('Ciklum', 'Empowering collaboration', 'Odessa, Ukraine');
INSERT INTO companies (name, industry, location) VALUES ('Sigma', '', 'Odessa, Ukraine');

/*==============================================================*/
/* SET TEAMS DATA                                               */
/*==============================================================*/
INSERT INTO teams (project_manager_id, team_leader_id, name) VALUES (1, 1, 'Struts');
INSERT INTO teams (project_manager_id, team_leader_id, name) VALUES (1, 2, 'APPDS');
INSERT INTO teams (project_manager_id, team_leader_id, name) VALUES (1, 3, 'TESTs');
INSERT INTO teams (project_manager_id, team_leader_id, name) VALUES (null, 4, 'Corvina');


/*==============================================================*/
/* SET PROJECTS DATA                                            */
/*==============================================================*/
INSERT INTO projects (company_id, team_id, name) VALUES (1, 3, 'Second Life');
INSERT INTO projects (company_id, team_id, name) VALUES (1, 2, 'Tivo Rhapsody');
INSERT INTO projects (company_id, team_id, name) VALUES (1, 1, 'Comcast');
INSERT INTO projects (company_id, team_id, name) VALUES (1, 4, 'Aruba');
INSERT INTO projects (company_id, team_id, name) VALUES (1, null, 'eHarmony');
INSERT INTO projects (company_id, team_id, name) VALUES (1, null, 'Destination-U');
INSERT INTO projects (company_id, team_id, name) VALUES (1, null, 'Asempra');

/*==============================================================*/
/* SET JOBS DATA                                                */
/*==============================================================*/
--evan
INSERT INTO jobs (employee_id, team_id, status) VALUES (1, 1, '1');
INSERT INTO jobs (employee_id, team_id, status) VALUES (1, 2, '1');
INSERT INTO jobs (employee_id, team_id, status) VALUES (1, 3, '1');

--val 
INSERT INTO jobs (employee_id, team_id, status) VALUES (2, 2, '1');

--ann
INSERT INTO jobs (employee_id, team_id, status) VALUES (3, 2, '0');
INSERT INTO jobs (employee_id, team_id, status) VALUES (3, 1, '1');

--barbara
INSERT INTO jobs (employee_id, team_id, status) VALUES (4, 1, '0');
INSERT INTO jobs (employee_id, team_id, status) VALUES (4, 2, '0');
INSERT INTO jobs (employee_id, team_id, status) VALUES (4, 3, '1');

--andrew
INSERT INTO jobs (employee_id, team_id, status) VALUES (5, 4, '1');

--nick
INSERT INTO jobs (employee_id, team_id, status) VALUES (6, 4, '1');

--marry
INSERT INTO jobs (employee_id, team_id, status) VALUES (7, 1, '1');
INSERT INTO jobs (employee_id, team_id, status) VALUES (7, 4, '1');

--allan
INSERT INTO jobs (employee_id, team_id, status) VALUES (8, 2, '0');

/*==============================================================*/
/* SET EXPERIENCE AND EXPERIENCE_SKILLS DATA                    */
/*==============================================================*/
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (1, 1, '2009-03-01', null, 'UI development. Bug fixing');
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (1, 7, '2006-02-01', '2006-07-01', 'New functionality implementation.');
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (1, 2, '2007-05-01', '2008-06-01', 'New functionality implementation and bug fixing');
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (1, 5, '2008-06-01', '2009-03-01', 'New functionality implementation and bug fixing');

INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (2, 1, '2011-03-01', null, 'Development, Bug fixing');

--Ann 
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (3, 1, '2007-07-01', null, 'Development, Bug fixing');

--Barbara 
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (4, 1, '2007-07-01', '2010-09-01', 'Development, Bug fixing');
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (4, 3, '2010-09-01', null, 'Development, Bug fixing');

--Andrew
INSERT INTO experience (employee_id, project_id, start_date, end_date, duties) VALUES (5, 4, '2007-07-01', null, 'Development, Bug fixing');

/*==============================================================*/
/* SET EXPERIENCE_SKILLS DATA                    		*/
/*==============================================================*/
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 1);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 25);

INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 1);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 10);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 11);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 13);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 15);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 1, 25);

INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 2, 1);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 2, 14);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 2, 18);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 2, 28);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 2, 29);

INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 3, 1);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 3, 17);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 3, 18);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 3, 13);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 3, 11);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (1, 3, 25);



INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 3);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 5);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 7);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 9);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 11);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 23);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 25);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 27);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (2, 4, 29);

--Ann
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 2);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 4);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 6);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 8);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 10);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 12);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 14);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 27);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (3, 5, 29);

--Barbara
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 2);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 4);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 5);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 6);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 9);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 11);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 12);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 13);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 15);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 27);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 28);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 29);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 6, 30);

INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 2);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 4);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 5);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 6);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 9);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 11);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (4, 7, 12);

--Andrew
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 1);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 2);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 3);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 4);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 6);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 16);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 12);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 17);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 15);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 22);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 23);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 24);
INSERT INTO experience_skills (employee_id, experience_id, skill_description_id) VALUES (5, 8, 31);

/*==============================================================*/
/* SET COMPUTERS DATA                                           */
/*==============================================================*/
INSERT INTO computers (employee_id, motherboard, ram, processor, hard_drive, audio, video, keyboard, mice) VALUES (2, 'Intel nForce', 'SAMSUNG 8GB DDR3', 'Intel core duo 2 2GHz', 'Samsung 300GB', 'SB Audigy 2NX', 'Voodoo 3', 'Mitsumi', 'Logitech');
INSERT INTO computers (employee_id, motherboard, ram, processor, hard_drive, audio, video, keyboard, mice) VALUES (4, 'MSI nForce2', 'SAMSUNG 8GB DDR2', 'Intel core duo 1.7GHz', 'Samsung 120GB', '', '', 'Apple keyboard', 'Razor');
INSERT INTO computers (employee_id, motherboard, ram, processor, hard_drive, audio, video, keyboard, mice) VALUES (5, 'MSI nForce2', 'SAMSUNG 8GB DDR2', 'Intel core duo 1.7GHz', 'Samsung 120GB', '', '', 'Microsoft', 'Logitech');
INSERT INTO computers (employee_id, motherboard, ram, processor, hard_drive, audio, video, keyboard, mice) VALUES (6, 'Acer nForce3', 'SAMSUNG 16GB DDR3', 'Intel core i7 1.7GHz', 'Samsung 500GB', '', 'ATI Radeon 240', 'NoName', 'Logitech');
INSERT INTO computers (employee_id, motherboard, ram, processor, hard_drive, audio, video, keyboard, mice) VALUES (7, 'Asus bla-bla', 'SAMSUNG 16GB DDR3', 'Intel core i5 1.5GHz', 'Seagate 500GB', '', 'ATI Radeon 240', 'NoName', 'Logitech');
INSERT INTO computers (employee_id, motherboard, ram, processor, hard_drive, audio, video, keyboard, mice) VALUES (8, 'Asus bla-bla', 'SAMSUNG 16GB DDR3', 'Intel core i5 1.5GHz', 'Seagate 500GB', '', 'ATI Radeon 240',  'NoName', 'Logitech');

/*==============================================================*/
/* SET MONITORS DATA                                            */
/*==============================================================*/
INSERT INTO monitors (employee_id, name, model) VALUES (2, 'Samsung', 'SyncMaster 19');
INSERT INTO monitors (employee_id, name, model) VALUES (4, 'Samsung', 'T710N');
INSERT INTO monitors (employee_id, name, model) VALUES (5, 'Samsung', 'T710N');
INSERT INTO monitors (employee_id, name, model) VALUES (6, 'Dell', 'U2412M');
INSERT INTO monitors (employee_id, name, model) VALUES (7, 'Dell', 'U2412M');
INSERT INTO monitors (employee_id, name, model) VALUES (8, 'Dell', 'U2412M');
INSERT INTO monitors (employee_id, name, model) VALUES (1, 'Dell', 'U2412M');
INSERT INTO monitors (employee_id, name, model) VALUES (1, 'Samsung', 'T710N');
INSERT INTO monitors (employee_id, name, model) VALUES (3, 'Apple', 'Cinema display 28');
INSERT INTO monitors (employee_id, name, model) VALUES (3, 'Samsung', 'T710N');

/*==============================================================*/
/* SET LAPTOPS DATA                                             */
/*==============================================================*/
INSERT INTO laptops (employee_id, name, model) VALUES (1, 'DELL', 'inspiron');
INSERT INTO laptops (employee_id, name, model) VALUES (3, 'Apple', 'mac book pro');

/*==============================================================*/
/* SET ROLES DATA                                               */
/*==============================================================*/
INSERT INTO roles (name) VALUES ('ROLE_ADMIN');
INSERT INTO roles (name) VALUES ('ROLE_RECRUITING');
INSERT INTO roles (name) VALUES ('ROLE_ACCOUNTER');
INSERT INTO roles (name) VALUES ('ROLE_SECRETARY');
INSERT INTO roles (name) VALUES ('ROLE_STEWARD');
INSERT INTO roles (name) VALUES ('ROLE_IT_ADMIN');
INSERT INTO roles (name) VALUES ('ROLE_USER');
INSERT INTO roles (name) VALUES ('ROLE_ANONYMOUS');

/*==============================================================*/
/* SET PERMISSIONS DATA                                         */
/*==============================================================*/

INSERT INTO permissions (employee_id, role_id) VALUES (9, 2); -- HR
INSERT INTO permissions (employee_id, role_id) VALUES (10, 3); -- Accounter
INSERT INTO permissions (employee_id, role_id) VALUES (11, 4); -- Secretary
INSERT INTO permissions (employee_id, role_id) VALUES (12, 5); -- Steward

INSERT INTO permissions (employee_id, role_id) VALUES (1, 6); -- IT Admin
INSERT INTO permissions (employee_id, role_id) VALUES (1, 7);

INSERT INTO permissions (employee_id, role_id) VALUES (2, 1); 
INSERT INTO permissions (employee_id, role_id) VALUES (3, 7);
INSERT INTO permissions (employee_id, role_id) VALUES (4, 7);
INSERT INTO permissions (employee_id, role_id) VALUES (5, 7);
INSERT INTO permissions (employee_id, role_id) VALUES (6, 7);
INSERT INTO permissions (employee_id, role_id) VALUES (7, 7);
INSERT INTO permissions (employee_id, role_id) VALUES (8, 8);

/*==============================================================*/
/* SET VACATIONS DATA                                           */
/*==============================================================*/

INSERT INTO vacations (employee_id, start_date, end_date) VALUES (2, '2008-07-01', '2008-07-12'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (2, '2009-05-11', '2009-05-22'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (3, '2010-04-01', '2010-05-01'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (3, '2011-07-11', '2011-08-01'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (4, '2010-03-14', '2010-04-12'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (4, '2011-05-04', '2011-05-12'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (4, '2012-05-04', '2012-05-05'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (4, '2012-08-11', '2012-08-12'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (5, '2011-07-21', '2011-08-12'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (5, '2012-03-01', '2012-03-13'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (5, '2013-02-27', '2013-03-09'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (6, '2013-06-22', '2013-07-02'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (6, '2013-11-22', '2014-01-05'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (7, '2012-08-17', '2012-09-17'); 
INSERT INTO vacations (employee_id, start_date, end_date) VALUES (8, '2012-10-24', '2012-11-16'); 

/*==============================================================*/
/* SET EMPLOYEES INFO DATA                                      */
/*==============================================================*/

INSERT INTO employees_info (employee_id, birthday, childs) VALUES (1, '1982-10-24', 2); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (2, '1984-04-14', 1); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (3, '1984-07-11', 1); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (4, '1987-11-27', 0); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (5, '1992-05-17', 0); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (6, '1982-02-03', 3); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (7, '1983-08-13', 1); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (8, '1986-12-18', 0); 

INSERT INTO employees_info (employee_id, birthday, childs) VALUES (9, '1976-03-13', 2); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (10, '1968-11-02', 1); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (11, '1976-07-10', 1); 
INSERT INTO employees_info (employee_id, birthday, childs) VALUES (12, '1980-01-18', 1); 

/*==============================================================*/
/* SET OFFICES DATA	                                        */
/*==============================================================*/

INSERT INTO offices (name, address, email) VALUES ('Svobody 1', 'st.Svobody 15, ap. 12', 'svobody1@thecompany.com'); 
INSERT INTO offices (name, address, email) VALUES ('Svobody 2', 'st.Svobody 15, ap. 14', 'svobody2@thecompany.com'); 
INSERT INTO offices (name, address, email) VALUES ('Gagarina', 'st.Gagarina 118, ap. 2', 'gagarina@thecompany.com'); 

/*==============================================================*/
/* SET LOCATIONS DATA	                                        */
/*==============================================================*/

INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (1, 1, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (2, 1, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (3, 1, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (4, 1, 2, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (5, 1, 2, '2012-03-01'); 

INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (6, 2, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (7, 2, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (8, 2, 1, '2012-03-01'); 

INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (9, 3, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (10, 3, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (11, 3, 1, '2012-03-01'); 
INSERT INTO locations (employee_id, office_id, room, start_date) VALUES (12, 3, 1, '2012-03-01'); 


